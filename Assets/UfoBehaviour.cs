﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UfoBehaviour : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(GameStats.GameIsOver){
            Rigidbody rb = this.gameObject.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
        }
	}
}
