﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour {

    public GameObject spawnArea;
    public GameObject objectToSpawn;
    public float spawnTime = 1f;
    public float enemySpeed = 200;

    private bool canSpawnEnemy = true;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (canSpawnEnemy && !GameStats.GameIsOver)
        {
            this.StartCoroutine(SpawnEnemy());
        }
	}

    IEnumerator SpawnEnemy()
    {
        canSpawnEnemy = false;
        Vector3 pos = new Vector3(spawnArea.transform.position.x+Random.Range(-5, 3), spawnArea.transform.position.y, spawnArea.transform.position.z);
        GameObject enemy = Instantiate(objectToSpawn, pos, Quaternion.Euler(270, 270, 0));
        Rigidbody rb = enemy.GetComponent<Rigidbody>();
        rb.AddForce(0, 0, -enemySpeed, ForceMode.Acceleration);
        yield return new WaitForSeconds(spawnTime);
        canSpawnEnemy = true;
    }
}
