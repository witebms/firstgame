﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour {

    public Rigidbody player;
    public Vector3 sidewayMovement;
    public float movementSpeed = 5f;
    public Camera playerCam;

	// Use this for initialization
	void Start () {
        Input.gyro.enabled = true;
        Debug.Log("GYRO enabled: "+ SystemInfo.supportsGyroscope);
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if(GameStats.GameIsOver){
            return;
        }

        Vector3 viewPos = playerCam.WorldToViewportPoint(player.transform.position);

        if (Input.GetKey("d") && viewPos.x <= 1) {
            Vector3 direction = ((sidewayMovement + player.transform.position)-player.transform.position).normalized;
            player.MovePosition(Vector3.Lerp(player.position, player.position + sidewayMovement, Time.fixedDeltaTime / movementSpeed));// AddForce(sidewayMovement*Time.deltaTime, ForceMode.VelocityChange);
        }
        if (Input.GetKey("a") && viewPos.x >= 0)
        {
            Vector3 direction = (sidewayMovement - player.transform.position).normalized;
            player.MovePosition(Vector3.Lerp(player.position, player.position - sidewayMovement, Time.fixedDeltaTime / movementSpeed));// AddForce(sidewayMovement*Time.deltaTime, ForceMode.VelocityChange);
        }

        /*if (viewPos.x < 0)
        {
            player.MovePosition(new Vector3(0, player.transform.position.y, player.transform.position.z));
        }
        }

        if (viewPos.x > 1)
        {
            player.MovePosition(new Vector3(1, player.transform.position.y, player.transform.position.z));
        }*/

        //player.AddForce(Input.gyro.gravity.x * Time.deltaTime, 0, Input.gyro.gravity.y * Time.deltaTime, ForceMode.VelocityChange);

        if(SystemInfo.supportsGyroscope){
            Vector3 oldPosition = this.transform.position;
            Vector3 newPosition = oldPosition + new Vector3(Input.gyro.gravity.x * movementSpeed * Time.deltaTime, 0, 0);

            if (viewPos.x <= 1 && viewPos.x >= 0)
            {
                player.MovePosition(newPosition);
            }
        }
        //player.AddForce(Input.gyro.rotationRate*Time.deltaTime, ForceMode.VelocityChange);

        /*Debug.Log("attitude:"+Input.gyro.attitude);
         Debug.Log("gravity" + Input.gyro.gravity);
         Debug.Log("rotationRate" + Input.gyro.rotationRate);
         Debug.Log("userAcceleration" + Input.gyro.userAcceleration);
*/
         
    }
}
