﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitDetection : MonoBehaviour {

    private Rigidbody player;
    public int damage;
    public GameObject damageParticleEffect;

    public void SetPlayer(Rigidbody _player){
        player = _player;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (player != null && player.transform.position.z <= this.transform.position.z - 100)
        {
            Destroy(this.gameObject);
        }
        if(GameStats.GameIsOver){
            this.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag != "Player"){
            Health enemyHealth = collision.collider.GetComponent<Health>();
            enemyHealth.reduceHealth(damage);
            Destroy(this.gameObject);
            if (damageParticleEffect != null)
            {
                GameObject effectIns = Instantiate(damageParticleEffect, transform.position, transform.rotation);
                Destroy(effectIns, 2f);
            }

        }
    }
}
