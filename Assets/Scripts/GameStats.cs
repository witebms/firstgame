﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameStats : MonoBehaviour {

    public Text scoreLabel;

    public static bool GameIsOver;
    private static int score;
    public GameObject GameOverScreen;

    public static void AddToScore(int amount){
        score += amount;
    }

	// Use this for initialization
	void Start () {
        GameStats.GameIsOver = false;
        GameStats.score = 0;
    }

    // Update is called once per frame
    void Update () {
        scoreLabel.text = score.ToString();
        if(GameStats.GameIsOver){
            GetComponent<Spawn>().enabled = false;
            GameOverScreen.SetActive(true);
        } else {
            GameOverScreen.SetActive(false);
        }
    }

    public void OnTryAgainClicked(){
        GameStats.GameIsOver = false;
    }
}
