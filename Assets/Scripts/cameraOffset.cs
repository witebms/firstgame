﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraOffset : MonoBehaviour {

    public Transform cameraTransform;
    public Vector3 offset;
	
	// Update is called once per frame
	void Update () {
        cameraTransform.position = new Vector3(cameraTransform.position.x, cameraTransform.position.y, this.transform.position.z-offset.z);
	}
}
