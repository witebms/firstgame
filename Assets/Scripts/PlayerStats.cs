﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {

    public float health = 10;
    private float initHealth;
    public Image playerHealthUIBar;

	// Use this for initialization
	void Start () {
        initHealth = health;
        playerHealthUIBar.fillAmount = 1;
    }

    // Update is called once per frame
    void Update () {
        if(health <= 0){
            GameStats.GameIsOver = true;
            Destroy(this.gameObject);
        }
	}

    public void DecreaseHealthBy(int decreaseAmount){
        health -= decreaseAmount;
        playerHealthUIBar.fillAmount = health / initHealth;
    }

}
