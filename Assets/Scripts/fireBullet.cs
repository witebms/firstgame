﻿using UnityEngine;
using System.Collections;

public class fireBullet : MonoBehaviour {

    public Rigidbody bullet;
    public float bulletSpeed = 3000f;
    public float shootSpeedWait = 1f;
    public Transform bulletSpawnPoint;
    public bool autoFire = false;

    public Quaternion rotation;

    private bool allowFire = true;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if(((Input.GetMouseButton(0) || Input.GetKey("space")) && allowFire) || ( allowFire && autoFire)) {
            if(!GameStats.GameIsOver){
                StartCoroutine(Fire());
            }
        }

	}

    IEnumerator Fire()
    {
        allowFire = false;
        Rigidbody rocketClone = (Rigidbody)Instantiate(bullet, bulletSpawnPoint.position, rotation);
        rocketClone.AddForce(0, 0, bulletSpeed, ForceMode.Acceleration);
        hitDetection hit = rocketClone.GetComponent<hitDetection>();
        hit.SetPlayer(this.gameObject.GetComponent<Rigidbody>());
        yield return new WaitForSeconds(shootSpeedWait);
        allowFire = true;
    }
}
