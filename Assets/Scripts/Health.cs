﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour {


    public int health;
    public Image healthBar;

    private int startHealth;

    public void reduceHealth(int damage){
        health -= damage;
    }

	// Use this for initialization
	void Start () {
        startHealth = health;
	}
	
	// Update is called once per frame
	void Update () {
        healthBar.fillAmount = (float)health / (float)startHealth;
        if(health <= 0){
            Destroy(this.gameObject);
            GameStats.AddToScore(1);
        }
	}
}
