﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerStats))]
public class CollisionOnPlayer : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.collider.tag == "enemy" || collision.collider.tag == "bullet"){
            Destroy(collision.collider.gameObject);
            PlayerStats playerStats = GetComponent<PlayerStats>();
            playerStats.DecreaseHealthBy(1);
            Debug.Log("Hit Enemy");
        }
        //Destroy(this);
    }
}
